---
layout: default
title: Python
parent: asdf
has_children: true
permalink: /docs/asdf/python/
---

## Table of contents
{: .no_toc .text-delta }
1. TOC
{:toc}

# Python

Instalação do Python via asdf

## Pré requisitos

```bash
apt install build-essential libffi-dev zlib1g-dev libssl-dev libreadline-dev libncursesw5-dev libgdbm-dev libc6-dev zlib1g-dev libsqlite3-dev libssl-dev openssl liblzma-dev xz-utils libbz2-dev curl git tk-dev
```

## Instalação

```bash
asdf plugin add python
asdf install python latest
asdf global python latest
asdf reshim
```

```bash
python3 -m pip install pip -U
python3 -m pip install wheel -U
python3 -m pip install setuptools -U
```
