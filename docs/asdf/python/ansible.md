---
layout: default
title: Ansible
parent: Python
grand_parent: asdf
permalink: /docs/asdf/python/ansible/
---

## Table of contents
{: .no_toc .text-delta}
1. TOC
{:toc}

# Ansible

Gestão de configuração sem a necessidade de agente.
{: .fs-6 .fw-300 }


## Um pouco de história

Ansible passou por uma reestruturação "pesada" que resultou em:

- **ansible-core**:
  - antigo ansible-base, consiste no básico (binários e outras ferramentas) para executar o Ansible
  
- **ansible-collections**:
  - coleções de roles, módulos, libraries, plugins etc. Permitindo que seus desenvolvedores possam lançar seus releases sem depender do projeto principal.

- **Ansible**:
  - pacote que inclui o ansible-core mais 41 collections (as principais, lembrando o antigo ansible de antes da reestruturação)


Mais em: [Thoughts on restructuring](https://www.ansible.com/blog/thoughts-on-restructuring-the-ansible-project){:target="_blank"}

## Instalação

Após a restruturação, passei a instalar somente o ansible-core:

```bash
python3 -m pip install ansible-core ansible-lint -U
asdf reshim
```

## Configuração

```bash
mkdir -p $HOME/.ansible
touch $HOME/.ansible/ansible.log
chown $USER:$GROUP $HOME/.ansible/ansible.log
```

Configurações gerais em: `$HOME/.ansible.cfg` com:
```ini
# config file for ansible
# https://docs.ansible.com/ansible/latest/reference_appendices/config.html
# ========================================================================

[defaults]
# If set, configures the path to the Vault password file as an alternative to
# specifying --vault-password-file on the command line.
#vault_password_file = ./.ansible/vault_password_file

roles_path = ./.ansible/roles
collections_paths = ./.ansible/collections

action_plugins     = ./.ansible/plugins/action
become_plugins     = ./.ansible/plugins/become
cache_plugins      = ./.ansible/plugins/cache
callback_plugins   = ./.ansible/plugins/callback
connection_plugins = ./.ansible/plugins/connection
lookup_plugins     = ./.ansible/plugins/lookup
inventory_plugins  = ./.ansible/plugins/inventory
vars_plugins       = ./.ansible/plugins/vars
filter_plugins     = ./.ansible/plugins/filter
test_plugins       = ./.ansible/plugins/test
terminal_plugins   = ./.ansible/plugins/terminal
strategy_plugins   = ./.ansible/plugins/strategy

strategy = free
forks = 30

# logging is off by default unless this path is defined
# if so defined, consider logrotate
log_path = ./.ansible/ansible.log

# if set to a persistent type (not 'memory', for example 'redis') fact values
# from previous runs in Ansible will be stored.  This may be useful when
# wanting to use, for example, IP information from one group of servers
# without having to talk to them in the same playbook run to get their
# current IP information.
fact_caching = jsonfile

#This option tells Ansible where to cache facts. The value is plugin dependent.
#For the jsonfile plugin, it should be a path to a local directory.
#For the redis plugin, the value is a host:port:database triplet: fact_caching_connection = localhost:6379:0
fact_caching_connection = ./.ansible/facts

# plays will gather facts by default, which contain information about
# the remote system.
#
# smart - gather by default, but don't regather if already gathered
# implicit - gather by default, turn off with gather_facts: False
# explicit - do not gather by default, must say gather_facts: True
gathering = smart

# controls the compression level of variables sent to
# worker processes. At the default of 0, no compression
# is used. This value must be an integer from 0 to 9.
#var_compression_level = 9

# SSH timeout
timeout = 10

host_key_checking = False

ansible_managed = Ansible managed

# https://docs.ansible.com/ansible/latest/reference_appendices/config.html#transform-invalid-group-chars
force_valid_group_names = ignore

[inventory]
enable_plugins = host_list, script, auto, yaml, ini, toml, amazon.aws.aws_ec2

# ignore these extensions when parsing a directory as inventory source
ignore_extensions = .pyc, .pyo, .swp, .bak, ~, .rpm, .md, .txt, ~, .orig, .cfg, .retry

[ssh_connection]
# ssh arguments to use
# Leaving off ControlPersist will result in poor performance, so use
# paramiko on older platforms rather than removing it, -C controls compression use
ssh_args = -C -o ControlMaster=auto -o ControlPersist=180s

# Enabling pipelining reduces the number of SSH operations required to
# execute a module on the remote server. This can result in a significant
# performance improvement when enabled, however when using "sudo:" you must
# first disable 'requiretty' in /etc/sudoers
#
# By default, this option is disabled to preserve compatibility with
# sudoers configurations that have requiretty (the default on many distros).
#
pipelining = true
```

# Ansible Collections

Defino quais collections instalar em um arquivo yaml: `$HOME/.ansible/collections_reqs.yaml` com:

```yaml
---
collections:
  - amazon.aws
  - ansible.netcommon
  - ansible.posix
  - ansible.utils
  - ansible.windows
  - awx.awx
  - azure.azcollection
  - chocolatey.chocolatey
  - cloud.common
  - community.aws
  - community.azure
  - community.ciscosmb
  - community.crypto
  - community.digitalocean
  - community.dns
  - community.docker
  - community.fortios
  - community.general
  - community.google
  - community.grafana
  - community.hashi_vault
  - community.hrobot
  - community.kubernetes
  - community.kubevirt
  - community.libvirt
  - community.mongodb
  - community.mysql
  - community.network
  - community.okd
  - community.postgresql
  - community.proxysql
  - community.rabbitmq
  - community.routeros
  - community.skydive
  - community.sops
  - community.vmware
  - community.windows
  - community.zabbix
  - containers.podman
  - google.cloud
  - kubernetes.core
```

E para instalar:

```bash
ansible-galaxy collection install --requirements-file $HOME/.ansible/collections_reqs.yaml --force
```
