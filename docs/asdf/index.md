---
layout: default
title: asdf
has_children: true
permalink: /docs/asdf/
---

# asdf

Gerenciador de versão de runtimes usando a mesma CLI.
Muito fácil e prático de usar.
{: .fs-6 .fw-300 }

Instalação se resume em:
```bash
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.2
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc
```

Mais informações em: [asdf](https://asdf-vm.com/){:target="_blank"}
