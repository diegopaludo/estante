![Build Status](https://gitlab.com/diegopaludo/estante/badges/main/pipeline.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll?color=blue&label=jekyll&style=flat)
![Just-the-docs theme](https://img.shields.io/gem/v/just-the-docs?color=blue&label=just-the-docs&style=flat)

# Estante

Algumas anotações...

...das experiências vividas nessa jornada de sysadmin/SRE
