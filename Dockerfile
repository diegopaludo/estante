FROM ruby:3.1

WORKDIR /app
COPY . .

RUN echo "gem: --no-ri --no-rdoc" > ~/.gemrc && \
  bundle install
